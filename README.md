# message manager

# Installation
+ clone laradock: git clone https://github.com/laradock/laradock.git
+ cd laradock
+ create new web folder and cd into web
+ clone the repo.
+ cd ../
+ `cp .env.example .env`
+ update .env file: POSTGRES_DB, POSTGRES_USER, POSTGRES_PASSWORD, POSTGRES_PORT, APP_CODE_PATH_HOST=./web
+ cd nginx/sites and edit default.conf(or copy default.conf message_manager.conf) as below:

`server {

    listen 80;
    server_name message-manager.local;
    root /var/www/message-manager/public;
    index index.php index.html index.htm;

    location / {
         try_files $uri $uri/ /index.php$is_args$args;
    }

    location ~ \.php$ {
        try_files $uri /index.php =404;
        fastcgi_pass php-upstream;
        fastcgi_index index.php;
        fastcgi_buffers 16 16k;
        fastcgi_buffer_size 32k;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        #fixes timeouts
        fastcgi_read_timeout 600;
        include fastcgi_params;
    }

    location ~ /\.ht {
        deny all;
    }

    location /.well-known/acme-challenge/ {
        root /var/www/letsencrypt/;
        log_not_found off;
    }

    error_log /var/log/nginx/message_manager_error.log debug;
    access_log /var/log/nginx/message_manager_access.log;
}`
+ add this line to hosts file: 127.0.0.1 message-manager.local
+ Run `docker-compose up -d nginx postgres workspace` to start the containers, waiting for 10 munites
+ after build complete, run docker-compose exec workspace bash
+ cd message-manager and cp .env.example .env
+ edit .env file and update database the same with .env of laradock
+ composer install, php artisan key:generate, php artisan migrate --seed, php artisan passport:install( --force), npm install && npm run dev
+ Visit http://message-manager.local to see your Laravel application.

