@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading has-buttons clearfix">
                        <h1 class="panel-title pull-left">List Messages</h1>
                    </div>

                    <div class="panel-body">

                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Message Code</th>
                                    <th>Message Type Code</th>
                                    <th>Created at</th>
                                    <th>Updated at</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($messages as $message)
                                    <tr>
                                        <td>{{ $message->id }}</td>
                                        <td>{{ $message->message_code }}</td>
                                        <td>{{ $message->message_type_code }}</td>
                                        <td>{{ date('Y-m-d', strtotime($message->created_at)) }}</td>
                                        <td>{{ date('Y-m-d', strtotime($message->updated_at)) }}</td>
                                    </tr>
                                @empty
                                	No Messages!
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection