<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use illuminate\Support\Facades\Auth;
use Validator;

class MessagesController extends Controller
{
    public function index()
    {
	$messages = Message::where('user_id', Auth::id())->get();
	
	return view('messages.index', compact('messages'));
    }

    public function getMessage(Request $request)
    {
	$validator = Validator::make($request->all(),
	    [
	        'message_code' => 'required',
	        'message_type_code' => 'required'
	    ]
	);

	if ($validator->fails()) {
	    return response()->json(['success' => false, 'message' => $validator->errors()], 400);
	}

	$messages = Message::where('message_code', $request->message_code)
		     ->where('message_type_code', $request->message_type_code)
		     ->get(['id', 'content'])->toArray();

	if (empty($messages)) {
	    return response()->json(['success' => false, 'message' => 'data not found'], 404);
	}

	return response()->json(['success' => true, 'data' => $messages]);
    }
}
