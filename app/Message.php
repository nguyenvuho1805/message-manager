<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['id', 'message_code', 'content', 'message_type_code'];

    public function user() {
	return $this->belongsTo(\App\User::class);
    }
}
