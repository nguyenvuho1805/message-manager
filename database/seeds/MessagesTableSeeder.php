<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Message;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	Message::create([
	    'user_id' => 1,
	    'message_code' => 'MS01',
	    'content' => 'message content',
	    'message_type_code' => 'string'
	]);
	
	factory(Message::class, 5)->create();
    }
}
