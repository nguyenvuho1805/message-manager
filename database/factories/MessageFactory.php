<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Message;
use Faker\Generator as Faker;

$factory->define(Message::class, function (Faker $faker) {
    return [
        'user_id' => 1,
	'message_code' => 'MS'. $faker->postcode,
	'content' => $faker->text,
	'message_type_code' => 'string'
    ];
});
